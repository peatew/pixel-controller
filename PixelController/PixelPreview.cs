﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace PixelController
{
	class PixelPreview : UserControl
	{
		private ImageAttributes m_ImageAttributes;

		public Controllers.PixelSurface Pixels { get; set; }

		public PixelPreview()
		{
			// Set the control style to double buffer.
			this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
			this.SetStyle(ControlStyles.SupportsTransparentBackColor, false);
			this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

			m_ImageAttributes = new ImageAttributes();
			float[][] colorMatrixElements = 
			{ 
				new float[] {0,  0,  1,  0, 0}, // R
				new float[] {0,  1,  0,  0, 0}, // G
				new float[] {1,  0,  0,  0, 0}, // B
				new float[] {0,  0,  0,  1, 0}, // A 
				new float[] {0,  0,  0,  0, 1}
			}; 

			ColorMatrix colorMatrix = new ColorMatrix(colorMatrixElements);

			m_ImageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (Pixels == null)
			{
				e.Graphics.Clear(Color.Black);
			}
			else
			{
				e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor; 
				e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
				e.Graphics.DrawImage(Pixels.Bitmap, this.ClientRectangle, 0, 0, Pixels.Width, Pixels.Height, GraphicsUnit.Pixel, m_ImageAttributes); 
			}
		}

		public void UpdateBitmap()
		{
			Invalidate(); 
		}
	}
}
