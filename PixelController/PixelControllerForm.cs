﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Rug.Cmd;
using Rug.Cmd.Colors;
using PixelController.Controllers;

namespace PixelController
{
	public partial class PixelControllerForm : Form
	{
		IController m_PixelController;
		PixelSurface m_Pixels;
		OscController m_OscController; 

		public PixelControllerForm()
		{
			RC.Verbosity = ConsoleVerbosity.Normal;

			InitializeComponent();
		}

		private void PixelControllerForm_Load(object sender, EventArgs e)
		{
			m_Pixels = new PixelSurface(120, 1); 
			m_PixelController = new Stripe();
			m_OscController = new OscController(); 

			propertyGrid1.SelectedObject = m_PixelController; 

			consoleControl.Buffer.BufferHeight = 50; 
			RC.App = consoleControl.Buffer;			
			RC.BackgroundColor = ConsoleColorExt.Black;
			RC.Theme = ConsoleColorTheme.Load(ConsoleColorDefaultThemes.Colorful);

			pixelPreview1.Pixels = m_Pixels;

			m_OscController.Connect(); 

			RC.WriteLine(ConsoleThemeColor.TitleText, "Welcome");			

			timer1.Enabled = true;			
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			m_PixelController.Run(m_Pixels, 0.1f);

			m_Pixels.CopyToByte24(); 
			m_Pixels.CopyToData(); 
			m_Pixels.CopyToBitmap(); 

			pixelPreview1.UpdateBitmap();

			m_OscController.Send(m_Pixels.PixelData); 
		}

		private void PixelControllerForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			m_Pixels.Dispose();

			m_OscController.Dispose(); 
		}
	}
}
