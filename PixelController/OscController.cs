﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Rug.Osc;
using Rug.Cmd;

namespace PixelController
{
	class OscSetup
	{
		public IPAddress IPAddress { get; set; }

		public int Port { get; set; }

		public string OscAddress { get; set; } 

		public OscSetup()
		{
			IPAddress = IPAddress.Parse("169.254.1.1"); // .Broadcast;
			Port = 9000;

			OscAddress = "/outputs/rgb/1"; 
		}
	}

	class OscController : IDisposable
	{
		private OscSender m_Sender;

		public OscSetup Setup { get; set; }

		public OscController()
		{
			Setup = new OscSetup();

			m_Sender = new OscSender(Setup.IPAddress, Setup.Port); 
		}

		public void Connect()
		{
			if (m_Sender.State == OscSocketState.Connected)
			{
				m_Sender.Close();
				m_Sender.Dispose(); 
			}

			RC.WriteLine(ConsoleThemeColor.TitleText2, "Connecting");
			RC.WriteLine(ConsoleThemeColor.SubText2, " IP Address : " + Setup.IPAddress);
			RC.WriteLine(ConsoleThemeColor.SubText2, " Port       : " + Setup.Port); 

			m_Sender = new OscSender(Setup.IPAddress, Setup.Port);

			try
			{
				m_Sender.Connect();

				RC.WriteLine(ConsoleThemeColor.TextGood, "Done"); 
			}
			catch (Exception ex)
			{				
				RC.WriteException(001, "Error connecting osc sender", ex);
			}					
		}

		public void Send(byte[] data)
		{
			if (m_Sender.State == OscSocketState.Connected)
			{
				m_Sender.Send(new OscMessage(Setup.OscAddress, data)); 
			}
		}

		public void Dispose()
		{
			m_Sender.Dispose(); 
		}
	}
}
