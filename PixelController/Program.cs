﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Rug.Cmd;

namespace PixelController
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			ConsoleColorState state = RC.ColorState;

			try
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Application.Run(new PixelControllerForm());
			}
			finally
			{
				RC.ColorState = state;
			}
		}
	}
}
