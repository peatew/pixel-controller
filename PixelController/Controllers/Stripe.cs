﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;

namespace PixelController.Controllers
{
	class Stripe : IController
	{
		float m_LinePosition = 0.0f;

		public float LineSpeed { get; set; }

		public float FadeAmount { get; set; }

		[Browsable(false)]
		public Pixel3F Fade { get; set; }

		[Browsable(false)]
		public Pixel3F Color { get; set; }

		public Color LineColor 
		{
			get
			{
				Pixel24B value24B = Color.Value24B;
				return System.Drawing.Color.FromArgb(value24B.R, value24B.G, value24B.B); 
			}
			set
			{
				Color = new Pixel24B(value.R, value.G, value.B).Value3F; 
			}
		}

		public Color FadeColor
		{
			get
			{
				Pixel24B value24B = Fade.Value24B;
				return System.Drawing.Color.FromArgb(value24B.R, value24B.G, value24B.B);
			}
			set
			{
				Fade = new Pixel24B(value.R, value.G, value.B).Value3F;
			}
		}

		public Stripe()
		{
			LineSpeed = 0.01f;
			FadeAmount = 0.2f;

			Color = new Pixel24B(255, 0, 0).Value3F; 
		}

		public void Run(PixelSurface surface, float delta)
		{
			surface.Fade3F(Fade * FadeAmount);

			m_LinePosition += delta * LineSpeed;

			m_LinePosition %= 1.0f;

			float xPos = m_LinePosition * (float)surface.Width;

			int lower = (int)xPos; 
			int upper = ((int)xPos + 1) % surface.Width; 

			float propInLower = xPos - lower; 

			for (int y = 0; y < surface.Height; y++)
			{
				surface.PixelsF[(y * surface.Width) + lower] += (Color * (1f - propInLower));
				surface.PixelsF[(y * surface.Width) + upper] += (Color * (propInLower));
			}
		}
	}
}
