﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;

namespace PixelController.Controllers
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct Pixel24B
	{
		public byte R;
		public byte G;
		public byte B;

		public Pixel3F Value3F
		{
			get { return new Pixel3F((float)R * Pixel3F.ValueScale, (float)G * Pixel3F.ValueScale, (float)B * Pixel3F.ValueScale); }
		}  

		public Pixel24B(byte r, byte g, byte b)
		{
			R = r;
			G = g;
			B = b;
		}
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct Pixel3F
	{
		public const float ValueScale = 1f / 255f;

		public float R;
		public float G;
		public float B;

		public Pixel24B Value24B 
		{
			get { return new Pixel24B(ClampValue(R), ClampValue(G), ClampValue(B)); } 
		}

		private static byte ClampValue(float value)
		{
			return (byte)Math.Max(Math.Min(value * 255f, 255f), 0f); 
		}

		public Pixel3F(float r, float g, float b)
		{
			R = r;
			G = g;
			B = b;
		}

		public static Pixel3F operator *(Pixel3F p1, Pixel3F p2)
		{
			return new Pixel3F(p1.R * p2.R, p1.G * p2.G, p1.B * p2.B);
		}

		public static Pixel3F operator *(Pixel3F p1, float scalar)
		{
			return new Pixel3F(p1.R * scalar, p1.G * scalar, p1.B * scalar);
		}

		public static Pixel3F operator /(Pixel3F p1, Pixel3F p2)
		{
			return new Pixel3F(p1.R / p2.R, p1.G / p2.G, p1.B / p2.B);
		}

		public static Pixel3F operator /(Pixel3F p1, float scalar)
		{
			return new Pixel3F(p1.R / scalar, p1.G / scalar, p1.B / scalar);
		}

		public static Pixel3F operator +(Pixel3F p1, Pixel3F p2)
		{
			return new Pixel3F(p1.R + p2.R, p1.G + p2.G, p1.B + p2.B);
		}

		public static Pixel3F operator +(Pixel3F p1, float scalar)
		{
			return new Pixel3F(p1.R + scalar, p1.G + scalar, p1.B + scalar);
		}

		public static Pixel3F operator -(Pixel3F p1, Pixel3F p2)
		{
			return new Pixel3F(p1.R - p2.R, p1.G - p2.G, p1.B - p2.B);
		}

		public static Pixel3F operator -(Pixel3F p1, float scalar)
		{
			return new Pixel3F(p1.R - scalar, p1.G - scalar, p1.B - scalar);
		}
	}

	class PixelSurface : IDisposable
	{
		public readonly int Width;
		public readonly int Height;

		public readonly Pixel3F[] PixelsF;

		public readonly Pixel24B[] Pixels;

		public readonly byte[] PixelData; 

		public readonly Bitmap Bitmap;

		public PixelSurface(int width, int height)
		{
			Width = width;
			Height = height;

			Pixels = new Pixel24B[Width * Height];
			PixelsF = new Pixel3F[Width * Height];
			PixelData = new byte[(Width * 3) * Height];
			
			Bitmap = new Bitmap(Width, Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb); 
		}

		public void CopyToByte24()
		{
			for (int i = 0; i < PixelsF.Length; i++)
			{
				Pixels[i] = (PixelsF[i] * 0.25f).Value24B; 
			}
		}

		public void CopyToData()
		{
			int sizeOfPixel = Marshal.SizeOf(typeof(Pixel24B));
			int length = sizeOfPixel * Pixels.Length;

			GCHandle handle = default(GCHandle);

			try
			{
				handle = GCHandle.Alloc(Pixels, GCHandleType.Pinned);
				
				IntPtr ptr = handle.AddrOfPinnedObject();
				
				Marshal.Copy(ptr, PixelData, 0, length);
			}
			finally
			{
				if (handle != default(GCHandle))
				{
					handle.Free();
				}
			}			
		}

		public void CopyToBitmap()
		{
			BitmapData sourceData =
					   Bitmap.LockBits(new Rectangle(0, 0,
					   Bitmap.Width, Bitmap.Height),
					   ImageLockMode.WriteOnly,
					   PixelFormat.Format24bppRgb);

			try 
			{
				Marshal.Copy(PixelData, 0, sourceData.Scan0, PixelData.Length);
			}
			finally
			{
				Bitmap.UnlockBits(sourceData);
			}				
		}

		public void Clear()
		{
			Array.Clear(Pixels, 0, Pixels.Length); 
		}

		public void ClearF()
		{
			Array.Clear(PixelsF, 0, PixelsF.Length);
		}

		public void Fade3F(float amount)
		{
			for (int i = 0; i < PixelsF.Length; i++)
			{
				PixelsF[i] *= amount; 
			}
		}

		public void Fade3F(Pixel3F amount)
		{
			for (int i = 0; i < PixelsF.Length; i++)
			{
				PixelsF[i] *= amount;
			}
		}

		public void Dispose()
		{
			Bitmap.Dispose(); 
		}

	}
}
