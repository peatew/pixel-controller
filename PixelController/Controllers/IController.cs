﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PixelController.Controllers
{
	interface IController
	{
		void Run(PixelSurface surface, float delta); 
	}
}
